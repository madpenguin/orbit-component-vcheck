# Orbit Component :: VCheck

#### This repository contains an Orbit Framework component which provides an example version checking component. This allows 'other' orbit applications to connect up and check for new software versions, and includes an UI to monitor activity.

More documentation to come ...