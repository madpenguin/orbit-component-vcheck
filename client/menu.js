import VCheck from '@/../node_modules/orbit-component-vcheck';
import SafetyCheckOutlined from '@vicons/material/SafetyCheckOutlined.js'
import { shallowRef } from 'vue'

export function menu (app, router, menu) {
    const IconVCheck = shallowRef(SafetyCheckOutlined)
    app.use(VCheck, {
        router: router,
        menu: menu,
        buttons: [{
            name: 'vcheck',
            text: 'VCheck',
            component: VCheck,
            icon: IconVCheck,
            pri: 3,
            path: '/vcheck',
            key: '100',
            meta: {
                root: 'vc1',
                host: location.host
            }
        }]
    })
}
