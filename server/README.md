# Orbit Component :: VCheck

#### This reusable component provides an example version checking system that allows other Orbit applications maintain a connection to a central server in order to be notified in real time of any version changes. Written more as a demo that the finished article, but good as a general purpose example.

